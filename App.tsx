/**
 * Sample React Native App with Typescript
 * https://gitlab.com/rn-workshops/step05
 *
 * @format
 */

import React, { Component } from "react";

import { FlatList, Image, StyleSheet, Text, View } from "react-native";

interface PictureCellProps {
  avatarUri: string;
  userName: string;
  location: string;
  imageUri: string;
  text: string;
}
class PictureCell extends Component<PictureCellProps> {
  render() {
    const { avatarUri, userName, location, imageUri, text } = this.props;
    return (
      <View>
        <View style={{ flexDirection: "row" }}>
          <Image source={{ uri: avatarUri }} style={{ width: 32, height: 32, borderRadius: 16, margin: 8 }} />
          <View style={{ padding: 8, paddingLeft: 4 }}>
            <Text style={{ fontWeight: "600" }}>{userName}</Text>
            <Text style={{ color: "gray" }}>@ {location}</Text>
          </View>
        </View>
        <Image source={{ uri: imageUri }} style={{ height: 250 }} />
        <Text style={{ color: "black", fontSize: 14, margin: 8 }}>
          <Text style={{ fontWeight: "600" }}>{userName}</Text> {text}
        </Text>
      </View>
    );
  }
}

interface Props {}

interface State {
  posts: Array<{
    id: string;
    avatarUri: string;
    userName: string;
    location: string;
    imageUri: string;
    text: string;
  }>;
}

export default class App extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = { posts: [] };
  }
  componentDidMount() {
    fetch("https://api.graph.cool/simple/v1/cjmauvhjv05wy0135z9z0cl7k", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
      },
      body: '{"query":"{allPosts{id avatarUri userName location imageUri text}}"}',
    })
      .then(response => {
        return response.json();
      })
      .then(json => {
        this.setState({ posts: json.data.allPosts });
      });
  }

  render() {
    const { posts } = this.state;
    return (
      <View style={styles.container}>
        <FlatList
          keyExtractor={item => item.id}
          data={posts}
          renderItem={({ item }) => {
            return (
              <PictureCell
                avatarUri={item.avatarUri}
                userName={item.userName}
                location={item.location}
                imageUri={item.imageUri}
                text={item.text}
              />
            );
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 26,
    flex: 1,
    backgroundColor: "#F5FCFF",
  },
});
